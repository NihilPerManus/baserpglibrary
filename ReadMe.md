# BaseRpgLibrary??? #

If there is one thing I like playing, it's RPGs! Who doesn't? This library was designed for me to be able to quickly design games using generic nomenclature elements found commonly in RPG games.
My goal is to create a simple to use library with which you can create your own game engines. 
You will find the library contains interfaces, abstract classes and extensions as the intention is to not contain anything so concrete that you are tied to a particular model of RPG (i.e. You don't have to adhere to DnD standards.)

### What is this repository for? ###

* BaseRPGLibrary - The actual sourcecode for the library
* BaseRPGLibrary_Tests - The testing solution used for testing the various elements of the library

### How do I get set up? ###

* Download the source to a location and compile.
* create your own project and reference the BaseRPGLibrary.DLL
* All resources fall under the Base.Rpg.* namespace

### Contribution guidelines ###

* Contributions are welcome. Create your pull requests and I will determine if it is generic enough for my liking to be included.
* As long as you follow the namespacing so there are no conflicts, everything is appropriately named and contains no specific references to actual games then all should be good

