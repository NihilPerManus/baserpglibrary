﻿using Base.Rpg.Generic.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Library
{
    public static class Register
    {
        static List<IUnique> _register = new List<IUnique>();
        
        static public T RegisterEntity<T>(T entrant) where T : IUnique
        {
            entrant.Id = _register.Count(); //give it an id equal to its position in the list
            _register.Add(entrant);


            Console.WriteLine($"Entrant registered as {entrant.Id}");
            return entrant;
        }

    }
}
