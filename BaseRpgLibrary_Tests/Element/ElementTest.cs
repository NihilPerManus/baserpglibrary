﻿using Base.Rpg.Elements;
using Base.Rpg.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.Library.Elements;

namespace Test.Library.Element
{
    [TestClass]
    public class ElementTest
    {
        IElement rock = Register.RegisterEntity(new RockElement());
        IElement paper = Register.RegisterEntity(new PaperElement());
        IElement scissors = Register.RegisterEntity(new ScissorsElement());

        public ElementTest()
        {
            


        }

        [TestMethod]
        public void PaperBeatsRock()
        {
            System.Console.WriteLine($"Paper to Rock: {paper.GetAffinity(rock)}");

            Assert.IsTrue(paper.GetAffinity(rock) > 1);
        }

        [TestMethod]
        public void ScissorsIsWeakAgainstRock()
        {
            System.Console.WriteLine($"Scissors to Rock: {scissors.GetAffinity(rock)}");
            Assert.IsTrue(scissors.GetAffinity(rock) < 1);
        }

        [TestMethod]
        public void PaperIsNullAgainstScissors()
        {
            System.Console.WriteLine($"Paper to Scissors: {paper.GetAffinity(scissors)}");
            Assert.IsTrue(paper.GetAffinity(scissors) == 0);
        }

    }
}
