﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Rpg.Attributes;
using Base.Rpg.Attributes.Elements;
using Base.Rpg.Attributes.Generic;
using Base.Rpg.Elements;
using Base.Rpg.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Library.Elements
{
    [Name("Paper")]
    [ElementAffinity(typeof(RockElement),2)]
    [ElementAffinity(typeof(ScissorsElement), 0f)] //paper can do shit all to Scissors 
    public class PaperElement : IElement
    {
        public long Id { get; set; }
    }

    [Name("Scissors")]
    [ElementAffinity(typeof(PaperElement),2f)]
    [ElementAffinity(typeof(RockElement), 0.1f)]
    public class ScissorsElement : IElement
    {
        public long Id { get; set; }
    }

    [Name("Rock")]
    [ElementAffinity(typeof(ScissorsElement), 2f)]
    [ElementAffinity(typeof(RockElement), 1)] //Not needed, but for clarity
    public class RockElement : IElement
    {
        public long Id { get; set; }
    }
}
