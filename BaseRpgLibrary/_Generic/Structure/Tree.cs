﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg._Generic.Structure
{
    public interface ITrunk<T>
    {
        IEnumerable<T> Children { get; set; }
    }
    public interface IBranch<TParent, TChildren>
    {
        TParent Parent { get; set; }
        IEnumerable<TChildren> Children { get; set; }
    }
    public interface ILeaf<TParent>
    {
        TParent Parent { get; set; }
    }
}
