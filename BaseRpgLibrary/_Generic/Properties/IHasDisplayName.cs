﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg._Generic.Properties
{
    //DisplayName should be used where the name is going to be presented to the user in a format different to its actual name
    //I.e you might have Name, and Surname, and you want the DisplayName to be their full name, or with a title.
    public interface IHasDisplayName
    {
        string DisplayName { get; set; }
    }
}
