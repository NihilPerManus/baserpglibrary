﻿using Base.Rpg.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg._Generic.Properties
{
    public interface IHasEffects
    {
        IEnumerable<IEffect> Effects { get; set; }
    }
}
