﻿using Base.Rpg._Attributes;
using Base.Rpg._Attributes.Elements;
using Base.Rpg._Attributes.Generic;
using Base.Rpg.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg._Extensions
{
    static public class ElementExtensions
    {
        
      

        public static string GetName(this IElement me)
        {
            return (me.GetType().GetCustomAttributes(true).FirstOrDefault(a => a is NameAttribute) as NameAttribute).Name;
        }

        public static float GetAffinity(this IElement me,IElement target)
        {
            var affinities = me.GetType()
                .GetCustomAttributes(true)
                .Where(a => a is ElementAffinity)
                .Select(a => a as ElementAffinity);

            affinities
                .ToList()
                .ForEach(a => Console.WriteLine($"Elemental Affinity found: {a.Element} with value: {a.BaseFactor}"));

            var affinity = affinities.FirstOrDefault(a => a.Element == target.GetType());

            return affinity == null ? 1 : affinity.BaseFactor;
        }



    }
}
