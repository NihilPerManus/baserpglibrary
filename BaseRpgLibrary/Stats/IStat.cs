﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Stats
{
    public interface IStat
    {
        long Value { get; set; }
    }
}
