﻿using Base.Rpg.Actors.Taxonomy;
using Base.Rpg.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Actors.Creature
{
    public abstract class Creature : IActor, ITaxonomy
    {
        public ILife Life { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IKingdom Kingdom { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IPhylum Phylum { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IClass Class { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IOrder Order { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IFamily Family { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public IGenus Genus { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ISpecies Species { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string Name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public long Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
