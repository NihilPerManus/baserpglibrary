﻿using Base.Rpg._Generic.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Actors
{
    //This class represents actors in a game. Players, NPCs, Shopkeepers, Monsters etc.
    public interface IActor : IHasName, IUnique
    {
    }
}
