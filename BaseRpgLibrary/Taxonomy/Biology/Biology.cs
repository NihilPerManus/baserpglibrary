﻿using Base.Rpg._Generic.Properties;
using Base.Rpg._Generic.Structure;
using Base.Rpg.Effects;
using Base.Rpg.Taxonomy.Biology;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Taxonomy.Biology
{

    public interface IExistence : IBiology, ITrunk<IPlane>{ }
    public interface IPlane : IBiology, IBranch<IExistence, ILife> { }
    public interface ILife : IBiology, IBranch<IPlane,IDomain> { }
    public interface IDomain : IBiology, IBranch<ILife, IKingdom>{ }
    public interface IKingdom : IBiology, IBranch<IDomain,IPhylum>{ }
    public interface IPhylum : IBiology, IBranch<IKingdom,IClass> { }
    public interface IClass : IBiology, IBranch<IPhylum,IOrder> { }
    public interface IOrder : IBiology, IBranch<IClass,IFamily> { }
    public interface IFamily : IBiology, IBranch<IOrder, IGenus> { }
    public interface IGenus : IBiology, IBranch<IFamily, ISpecies> { }
    public interface ISpecies : IBiology, ILeaf<IGenus> { }
    
    public interface IBiology : ITaxonomy
    {

    }
}
