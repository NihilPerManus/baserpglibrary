﻿using Base.Rpg._Generic.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Elements
{
    //Looks kinda useless but it's actually not. This is used for an extension methos which allows elements to comapre themselves with other elements.
    //Use this for testing Type Effectiveness. Or in this Library's terminology: Affinity
    public interface IElement : IUnique
    {

    }
}
