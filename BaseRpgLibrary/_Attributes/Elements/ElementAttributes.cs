﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg._Attributes.Elements
{

    
    [AttributeUsage(validOn:  AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class ElementAffinity : Attribute
    {
        public Type Element;
        public float BaseFactor; //how much this element. so 1 = normal. 2 = Twice, 0.5 means weak etc.

        public ElementAffinity(Type element, float baseFactor)
        {
            BaseFactor = baseFactor;
            Element = element;
        }
    }



}
