﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Random
{
    public interface IRandom
    {
        long GetRandom();
    }
}
