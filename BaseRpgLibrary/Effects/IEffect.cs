﻿using Base.Rpg._Generic.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Rpg.Effects
{
    //This class represents effects from attributes, consuming items, buffs etc.
    public interface IEffect : IHasName, IUnique
    {
    }
}
